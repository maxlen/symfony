<?php

namespace App\Repository;

use App\Entity\OrdersSettings;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrdersSettings|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrdersSettings|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrdersSettings[]    findAll()
 * @method OrdersSettings[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrdersSettingsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrdersSettings::class);
    }

    // /**
    //  * @return OrdersSettings[] Returns an array of OrdersSettings objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrdersSettings
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
