<?php

namespace App\Tests\Twig;

use PHPUnit\Framework\TestCase;
use App\Twig\AppExtension;

class SluggerTest extends TestCase
{
    /**
     * @dataProvider getSlugs
     * @param string $string
     * @param string $slug
     */
    public function testSlugify(string $string, string $slug)
    {
        $slugger = new AppExtension();
        $this->assertSame($slug, $slugger->slugify($string));
    }

    public function getSlugs()
    {
        return [
            ['Lorem Ipsum', 'lorem-ipsum'],
            [' Lorem Ipsum', 'lorem-ipsum'],
            ['!Lorem Ipsum!', 'lorem-ipsum'],
            ['lorem-ipsum', 'lorem-ipsum'],
            ['Children\'s books', 'childrens-books'],
            ['Adults 60+', 'adults-60'],
        ];
    }
}
